package ru.rgs.csvparser.api;

public class Request {

    private String clientName;
    private String contractDate;

    public Request(String clientName, String contractDate) {
        this.clientName = clientName;
        this.contractDate = contractDate;
    }

    public String getClientName() {
        return clientName;
    }

    public String getContractDate() {
        return contractDate;
    }

}
