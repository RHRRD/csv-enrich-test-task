package ru.rgs.csvparser.api;

public class CsvInput {

    private static final String DELIMITER = ",";

    private String firstName;
    private String lastName;
    private String middleName;
    private String contractDate;

    public CsvInput(String fileLine) {
        String[] split = fileLine.split(DELIMITER);
        if (split.length != 4) {
            return;
        }
        this.firstName = split[0];
        this.lastName = split[1];
        this.middleName = split[2];
        this.contractDate = split[3];
    }

    public String getContractDate() {
        return contractDate;
    }

    public String getClientName() {
        return firstName.toUpperCase() + " " + middleName.toUpperCase() + " " + lastName.toUpperCase();
    }
}
