package ru.rgs.csvparser.api;

public enum Status {
    COMPLETED,
    FAILED,
    NOT_FOUND;
}
