package ru.rgs.csvparser.api;

public class CsvOutput {

    private String clientName;
    private String contractDate;
    private String scoring;

    public CsvOutput(String clientName, String contractDate, String scoring) {
        this.clientName = clientName;
        this.contractDate = contractDate;
        this.scoring = scoring;
    }

    public String getStringForFile() {
        return clientName + "," + contractDate + "," + scoring;
    }
}
