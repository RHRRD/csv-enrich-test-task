package ru.rgs.csvparser.api;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    private Status status;
    private String description;
    private Double scoringValue;

    public Status getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public Double getScoringValue() {
        return scoringValue;
    }
}
