package ru.rgs.csvparser.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rgs.csvparser.api.CsvInput;
import ru.rgs.csvparser.api.CsvOutput;
import ru.rgs.csvparser.api.Request;
import ru.rgs.csvparser.api.Response;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CsvParserServiceImpl implements CsvParserService {

    private static Logger logger = LoggerFactory.getLogger(CsvParserServiceImpl.class);

    private static final String FILENAME = "filename.txt";
    private static final String FIRST_FILE_LINE = "CLIENT_NAME,CONTRACT_DATE,SCORING";

    @Autowired
    private EndpointService endpointService;

    private ConcurrentMap<Integer, CsvOutput> csvOutputMap;

    @Override
    public Path processCsv(Path source) {
        logger.debug(String.format("Path: %s", source));
        csvOutputMap = new ConcurrentHashMap<>();

        AtomicInteger counter = new AtomicInteger(0);
        List<Thread> threads = new ArrayList<>();
        for (String fileLine : getLinesFromPath(source)) {
            Thread thread = createThread(fileLine, counter.getAndIncrement());
            thread.start();
            threads.add(thread);
        }
        joinThreads(threads);
        createFileByMap(csvOutputMap);

        return new File(FILENAME).toPath();
    }

    private Thread createThread(String fileLine, int counter) {
        CsvInput input = new CsvInput(fileLine);
        String clientName = input.getClientName();
        String contractDate = input.getContractDate();
        Request request = new Request(clientName, contractDate);
        return new Thread(() ->
        {
            Response response = endpointService.sendRequest(request);
            CsvOutput output = new CsvOutput(clientName, contractDate, getScoringValue(response));
            logger.debug("CsvOutput: " + output);
            csvOutputMap.put(counter, output);
        });
    }

    private void joinThreads(List<Thread> threads) {
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                logger.error("Error during join Threads", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    private List<String> getLinesFromPath(Path source) {
        List<String> linesFromFile;
        try {
            linesFromFile = Files.readAllLines(source);
        } catch (IOException e) {
            logger.error("Error during parse file", e);
            return new ArrayList<>();
        }
        logger.trace(String.format("linesFromFile: %s", linesFromFile));
        if (linesFromFile.isEmpty()) {
            return new ArrayList<>();
        }
        linesFromFile.remove(0);
        return linesFromFile;
    }

    private void createFileByMap(Map<Integer, CsvOutput> outputList) {
        Path file = Paths.get(FILENAME);
        List<String> res = new ArrayList();
        res.add(FIRST_FILE_LINE);
        for (int i = 0; i < outputList.size(); i++) {
            res.add(outputList.get(i).getStringForFile());
        }
        try {
            Files.write(file, res, StandardCharsets.UTF_8);
        } catch (IOException e) {
            logger.error("Error during write file", e);
        }
    }

    private String getScoringValue(Response response) {
        switch (response.getStatus()) {
            case NOT_FOUND:
                return "не найден";
            case FAILED:
                return "ошибка обработки";
            case COMPLETED:
                return response.getScoringValue() != null ? response.getScoringValue().toString() : null;
            default:
                return null;
        }
    }

}
