package ru.rgs.csvparser.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import ru.rgs.csvparser.api.Request;
import ru.rgs.csvparser.api.Response;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class EndpointServiceImpl implements EndpointService {

    private static Logger logger = LoggerFactory.getLogger(EndpointServiceImpl.class);

    private static final String SCORE_URL = "http://localhost:8081/score";

    @Override
    public Response sendRequest(Request request) {
        logger.debug(String.format("Request: %s", request));
        String jsonRequest = requestToJson(request);
        logger.debug(String.format("jsonRequest: %s", request));
        try {
            ResponseEntity<Response> responseEntity = getRestTemplate().postForEntity(SCORE_URL, jsonRequest, Response.class);
            logger.debug(String.format("Response: %s", responseEntity));
            return responseEntity.getBody();
        } catch (HttpStatusCodeException exception) {
            logger.debug("HttpStatusCodeException for request: " + jsonRequest, exception);
            return jsonToResponse(exception.getResponseBodyAsString());
        }
    }

    private RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        return restTemplate;
    }

    private String requestToJson(Request request) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            logger.error("Error during parse Request to Json", e);
        }
        return null;
    }

    private Response jsonToResponse(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, Response.class);
        } catch (IOException e) {
            logger.error("Error during parse JSON to Response", e);
        }
        return new Response();
    }

}
