package ru.rgs.csvparser.service;

import ru.rgs.csvparser.api.Request;
import ru.rgs.csvparser.api.Response;

public interface EndpointService {

    /**
     * send post request to some system
     * @param request request to system
     * @return response from system
     */
    Response sendRequest(Request request);

}
